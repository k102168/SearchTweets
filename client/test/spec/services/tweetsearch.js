'use strict';

describe('Service: tweetSearch', function () {

  // load the service's module
  beforeEach(module('tweetApp'));

  // instantiate service
  var tweetSearch;
  beforeEach(inject(function (_tweetSearch_) {
    tweetSearch = _tweetSearch_;
  }));

  it('should do something', function () {
    expect(!!tweetSearch).toBe(true);
  });

});
