'use strict';

/**
 * @ngdoc function
 * @name tweetApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the tweetApp
 */
angular.module('tweetApp')
  .controller('MainCtrl',function ($scope,tweetSearch,$http) {
	tweetSearch.getTweets().then(function(res){
		$scope.tweets = res.data;
	})
	
	$http({
      url: '/tweetWord', // No need of IP address
      method: 'POST',
      data: $scope.tweetWord,
      headers: {'Content-Type': 'application/json'}
	}).then(function (httpResponse) {
        console.log('response:', httpResponse);
    })

	
  });
