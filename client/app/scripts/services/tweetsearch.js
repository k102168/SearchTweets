'use strict';

/**
 * @ngdoc service
 * @name tweetApp.tweetSearch
 * @description
 * # tweetSearch
 * Factory in the tweetApp.
 */
angular.module('tweetApp')
  .factory('tweetSearch',['$http',function ($http) {

    var baseApi={
      designations : "http://localhost:8080/tweets",
    };
       
    function getTweets(){
      return $http.get(baseApi.designations).then(function(res){return res},function(err){return err});
    }
    
        
    return {
      getTweets:getTweets
    };
  }]);